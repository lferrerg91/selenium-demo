package helpers;

/**
 * Created by luisferrer
 */

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SeleniumHelper {
    private WebDriver driver;

    public SeleniumHelper(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getElement(final By by) {
        return driver.findElement(by);
    }

    public List<WebElement> getElements(By by) {
        return driver.findElements(by);
    }

    public SeleniumHelper setText(By by, String value) {
        driver.findElement(by)
                .click();
        driver.findElement(by)
                .clear();
        driver.findElement(by)
                .sendKeys(value);
        return this;
    }

    public String getText(By by) {
        return driver.findElement(by)
                .getText();
    }

    public SeleniumHelper clickElement(By locator) {
        driver.findElement(locator)
                .click();
        return this;
    }

    public String getAttribute(By by, String attribute) {
        return getElement(by)
                .getAttribute(attribute);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getValue(By by) {
        return getAttribute(by, "value");
    }

    public WebDriverWait wait(int seconds)
    {
        return new WebDriverWait(driver, seconds);
    }

    public void tab()
    {
        Actions tab = new Actions(driver);
        tab.sendKeys(Keys.TAB).perform();
    }
}

