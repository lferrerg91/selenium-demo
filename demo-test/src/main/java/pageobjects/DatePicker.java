package pageobjects;

import helpers.SeleniumHelper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class DatePicker
{
    private SeleniumHelper sh;
    public final By datePicker = By.cssSelector(".datepicker.dropdown-menu:not([style=\"display: none;\"])"),
            prev = By.cssSelector(".datepicker.dropdown-menu:not([style=\"display: none;\"]) [style=\"display: block;\"] .prev"),
            next = By.cssSelector(".datepicker.dropdown-menu:not([style=\"display: none;\"]) [style=\"display: block;\"] .next"),
            days = By.cssSelector(".datepicker.dropdown-menu:not([style=\"display: none;\"]) [style=\"display: block;\"] tbody .day:not(.old)");

    private By field;

    public DatePicker(WebDriver driver, By field)
    {
        sh = new SeleniumHelper(driver);
        this.field = field;
    }

    private int getNumberOfMonths(int month, int year)
    {
        DateTime currentDate = DateTime.parse(sh.getElement(field).getAttribute("value"), DateTimeFormat.forPattern("dd MMM yyyy"));
        int currentYear = currentDate.getYear(),
                currentMonth = currentDate.getMonthOfYear(),
                monthsInYearsDiff = (year - currentYear) * 12,
                monthsDiff = month - currentMonth;

        return monthsInYearsDiff + monthsDiff;
    }

    private void iterateOverMonthsInDirection(int months, By direction)
    {
        for (int i = 0; i < months; i++)
        {
            sh.clickElement(direction);
        }
    }

    private void goToMonthYear(DateTime date)
    {
        int month = date.getMonthOfYear(),
                year = date.getYear(),
                numberOfMonths = getNumberOfMonths(month, year);

        if (numberOfMonths < 0)
        {
            numberOfMonths = Math.abs(numberOfMonths);
            iterateOverMonthsInDirection(numberOfMonths, prev);
        }

        else if (numberOfMonths > 0)
        {
            iterateOverMonthsInDirection(numberOfMonths, next);
        }
    }

    private void pickDay(int day)
    {
        List<WebElement> days = sh.getElements(this.days);
        days.get(day - 1).click();
        sh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(datePicker));
    }

    public void waitForDatePickerToAppear()
    {
        String opacity = "";
        sh.wait(3).until(ExpectedConditions.visibilityOfElementLocated(datePicker));
        while (opacity != null)
        {
            opacity = sh.getAttribute(datePicker, "opacity");
        }
    }

    private void assertDatePickedCorrect(DateTime date, By by)
    {
        String actualDate = sh.getValue(field),
                expectedDate = DateTimeFormat.forPattern("dd MMM yyyy").print(date);

        assert actualDate.equals(expectedDate) : "Actual Date: " + actualDate +"\n" +
                "Expected Date: " + expectedDate;
    }

    public void pickDate(DateTime date)
    {
        sh.clickElement(field);
        waitForDatePickerToAppear();
        goToMonthYear(date);

        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        pickDay(date.getDayOfMonth());
        assertDatePickedCorrect(date, field);
    }
}