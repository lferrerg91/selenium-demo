package pageobjects;

/**
 * Created by luisferrer
 */

import helpers.SeleniumHelper;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;



public class LoanCalculator extends LoadableComponent<LoanCalculator> {
    public final WebDriver driver;
    public final String siteUrl;
    public SeleniumHelper sh;

    public final By loanAmount = By.id("loanAmount"),
            loanTermInYears = By.id("years"),
            termInMonths = By.id("terms"),
            interestRatePerYear = By.id("interestRate"),
            todaysRates = By.cssSelector(".br-btn.br-green"),
            loanStartDate = By.id("loanStartDate"),
            calculate = By.id("calcButton"),
            monthlyPayments = By.id("mpay"),
            loanAmountError = By.cssSelector("span[for=\"loanAmount\"]"),
            loanTermInYearsError = By.cssSelector("span[for=\"years\"]"),
            termInMonthsError = By.cssSelector("span[for=\"terms\"]"),
            interestRatePerYearError = By.cssSelector("span[for=\"interestRate\"]");

    public LoanCalculator(WebDriver driver, SeleniumHelper sh) {
        this.driver = driver;
        siteUrl = "http://www.bankrate.com/calculators/mortgages/loan-calculator.aspx";
        this.sh = sh;
    }

    @Override
    public void load() {
        driver.get(siteUrl);
    }

    @Override
    protected void isLoaded() throws Error {
        assert driver.getCurrentUrl().equals(siteUrl) :
                "Not on Loan Calculator Page.\nOn page: '" + driver.getCurrentUrl() + "'";
    }

    public String getTitle() {
        return sh.getTitle();
    }

    public String getLoanAmount() {
        return sh.getValue(loanAmount);
    }

    public LoanCalculator setLoanAmount(String loanAmount) {
        sh.setText(this.loanAmount,loanAmount);
        return this;
    }

    public String getLoanTermInYears() {
        return sh.getValue(loanTermInYears);
    }

    public LoanCalculator setLoanTermInYears(String loanTermInYears) {
        sh.setText(this.loanTermInYears,loanTermInYears);
        return this;
    }

    public String getTermInMonths() {
        return sh.getValue(termInMonths);
    }

    public LoanCalculator setTermInMonths(String termInMonths) {
        sh.setText(this.termInMonths,termInMonths);
        return this;
    }

    public String getInterestRatePerYear() {
        return sh.getValue(interestRatePerYear);
    }

    public LoanCalculator setInterestRatePerYear(String interestRatePerYear) {
        sh.setText(this.interestRatePerYear,interestRatePerYear);
        return this;
    }

    public void clickTodaysRates() {
        sh.clickElement(todaysRates);
    }

    public String getLoanStartDate() {
        return sh.getValue(loanStartDate);
    }

    public LoanCalculator setLoanStartDate(DateTime loanStartDate) {
        DatePicker datePicker = new DatePicker(driver, this.loanStartDate);
        datePicker.pickDate(loanStartDate);
        return this;
    }

    public void clickCalculate() {
        sh.clickElement(calculate);
    }

    public String getLoanAmountError()
    {
        return sh.getText(loanAmountError);
    }

    public String getLoanTermInYearsError()
    {
        return sh.getText(loanTermInYearsError);
    }

    public String getTermInMonthsError()
    {
        return sh.getText(termInMonthsError);
    }

    public String getInterestRatePerYearError()
    {
        return sh.getText(interestRatePerYearError);
    }

    public String getMonthlyPayments(){
        return sh.getText(monthlyPayments);
    }
}