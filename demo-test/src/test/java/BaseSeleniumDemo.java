
import helpers.SeleniumHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.joda.time.DateTime;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created by luisferrer
 */
public abstract class BaseSeleniumDemo {
    public String browser, screenShotDirectory = "demo-test/src/test/resources/DemoScreenShots/";
    private File screenShotFolder = new File(screenShotDirectory);
    public static SeleniumHelper sh;
    public WebDriver driver;

    @Parameters({"browser"})
    @BeforeSuite
    protected void beforeSuite(@Optional("firefox") String browser){
        this.browser = browser;

        FileUtils.deleteQuietly(screenShotFolder);

        screenShotFolder.mkdir();
    }

    @BeforeClass
    public void beforeClass()
    {
        setUpDriver();
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResult, ITestContext testContext)
    {
        try
        {
            driver.switchTo().alert().dismiss();
        }
        catch(NoAlertPresentException e) {}

        if(testResult.getStatus() == ITestResult.SUCCESS)
        {

        }
        //TODO still calling set up driver when last test when invocation count greater than 1 fails
        else
        {
            System.out.println(screenShotDirectory);
            takeScreenShot(testResult);

            driver.quit();

            ITestNGMethod[] methods = testContext.getAllTestMethods();
            ITestNGMethod currentMethod = testResult.getMethod(),
                    lastMethod = methods[methods.length - 1];
            String lastMethodName = lastMethod.getMethodName();

            int invocationCount = currentMethod.getInvocationCount(),
                    parameterInvocationCount = currentMethod.getParameterInvocationCount(),
                    currentInvocationCount = currentMethod.getCurrentInvocationCount(),
                    numberOfPassedTests = testContext.getPassedTests().size(),
                    numberOfFailedTests = testContext.getFailedTests().size(),
                    numberOfTests = testContext.getAllTestMethods().length;
            boolean isCurrentMethodNameEqualToLastMethodName = currentMethod.getMethodName().equals(lastMethodName),
                    isCurrentInvocationCountEqualToInvocationCount = (invocationCount == currentInvocationCount),
                    isCurrentDataProviderInvocationEqualToDataProviderInvocations = (invocationCount == parameterInvocationCount);
            boolean isLastMethod = isCurrentMethodNameEqualToLastMethodName &&
                    isCurrentInvocationCountEqualToInvocationCount &&
                    isCurrentDataProviderInvocationEqualToDataProviderInvocations &&
                    (numberOfPassedTests + numberOfFailedTests == numberOfTests);

            if(!isLastMethod)
                setUpDriver();
        }
    }

    @AfterClass(alwaysRun = true)
    public void afterClass()
    {
        driver.quit();
    }

    public void setUpDriver()
    {
        browser = "firefox";
        if (this.browser == "firefox") {
            FirefoxProfile firefoxProfile = new FirefoxProfile();
            firefoxProfile.setPreference("reader.parse-on-load.enabled", false);
            driver = new FirefoxDriver(firefoxProfile);
        }
        driver.manage().window().maximize();
        sh = new SeleniumHelper(driver);
    }

    public void takeScreenShot(ITestResult testResult)
    {
        File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        String fileName = screenShotDirectory +
                testResult.getName() +
                "_" + ((RemoteWebDriver) driver).getCapabilities().getBrowserName() +
                "_" + new DateTime().toString("k-mm-ss") +
                ".jpg";

        System.out.println(fileName);

        try
        {
            FileUtils.moveFile(screenShot, new File(fileName));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
