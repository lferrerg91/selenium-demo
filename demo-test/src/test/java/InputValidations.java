import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.LoanCalculator;

import static org.testng.Assert.assertTrue;

/**
 * Created by luisferrer
 */
public class InputValidations extends BaseSeleniumDemo {

    public LoanCalculator loanCalculator;
    @BeforeMethod
    public void beforeMethod()
    {
        loanCalculator = new LoanCalculator(driver, sh);
        loanCalculator.load();
    }

    @Test
    public void checkLoanAmountInputField_Alphabet()
    {
        // Assert that Loan Amount Input Field displays error message when entering a non numeric character
        loanCalculator.setLoanAmount("a");
        sh.tab();

        assertError(loanCalculator.loanAmountError, loanCalculator.getLoanAmountError(), "Please enter a valid number.");
    }

    @Test
    public void checkLoanAmountInputField_SpecialCharacter()
    {
        loanCalculator.setLoanAmount("####");
        sh.tab();

        assertError(loanCalculator.loanAmountError, loanCalculator.getLoanAmountError(), "Please enter a valid number.");
    }

    @Test
    public void checkLoanAmountInputField_Zero()
    {
        // Assert that Loan Amount Input Field displays error message when entering a number that is not between 1 and 10000000
        loanCalculator.setLoanAmount("0");
        sh.tab();

        assertError(loanCalculator.loanAmountError, loanCalculator.getLoanAmountError(), "Please enter a value between 1 and 10000000");
    }

    @Test
    public void checkLoanAmountInputField_GreaterThanTenMillion()
    {
        loanCalculator.setLoanAmount("10000001");
        sh.tab();

        assertError(loanCalculator.loanAmountError, loanCalculator.getLoanAmountError(), "Please enter a value between 1 and 10000000");
    }

    @Test
    public void checkLoanAmountInputField_ErrorMessageDisappears()
    {
        loanCalculator.setLoanAmount("10000001");
        sh.tab();

        assertError(loanCalculator.loanAmountError, loanCalculator.getLoanAmountError(), "Please enter a value between 1 and 10000000");

        loanCalculator.setLoanAmount("30");
        sh.tab();

        sh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(loanCalculator.loanAmountError));
    }

    @Test
    public void checkLoanTermInYearsInputField_Alphabet()
    {
        // Assert that Term In Years Input Field displays error message when entering a non numeric character
        loanCalculator.setLoanTermInYears("a");
        sh.tab();

        assertError(loanCalculator.loanTermInYearsError, loanCalculator.getLoanTermInYearsError(), "Please enter a valid number.");
    }

    @Test
    public void checkLoanTermInYearsInputField_SpecialCharacter()
    {
        loanCalculator.setLoanTermInYears("####");
        sh.tab();

        assertError(loanCalculator.loanTermInYearsError, loanCalculator.getLoanTermInYearsError(), "Please enter a valid number.");
    }

    @Test
    public void checkLoanTermInYearsInputField_LessThanMinimum()
    {
        // Assert that Loan Term In Years Input Field displays error message when entering a number that is not between 0.083 and 40
        loanCalculator.setLoanTermInYears("0.082");
        sh.tab();

        assertError(loanCalculator.loanTermInYearsError, loanCalculator.getLoanTermInYearsError(), "Please enter a value between 0.083 and 40");
    }

    @Test
    public void checkLoanTermInYearsInputField_GreaterThanMaximum()
    {
        loanCalculator.setLoanTermInYears("41");
        sh.tab();

        assertError(loanCalculator.loanTermInYearsError, loanCalculator.getLoanTermInYearsError(), "Please enter a value between 0.083 and 40");
    }

    @Test
    public void checkLoanTermInYearsInputField_ErrorMesageDisappears()
    {
        loanCalculator.setLoanTermInYears("41");
        sh.tab();

        assertError(loanCalculator.loanTermInYearsError, loanCalculator.getLoanTermInYearsError(), "Please enter a value between 0.083 and 40");

        loanCalculator.setLoanTermInYears("30");
        sh.tab();

        sh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(loanCalculator.loanTermInYearsError));
    }

    @Test
    public void checkTermInMonthsInputField()
    {
        // Assert that Term In Months Input Field displays error message when entering a non numeric character
        loanCalculator.setTermInMonths("a");
        sh.tab();

        assertError(loanCalculator.termInMonthsError, loanCalculator.getTermInMonthsError(), "Please enter a valid number.");

        loanCalculator.setTermInMonths("####");
        sh.tab();

        assertError(loanCalculator.termInMonthsError, loanCalculator.getTermInMonthsError(), "Please enter a valid number.");

        // Assert that Loan Term In Months Input Field displays error message when entering a number that is not between 1 and 480
        loanCalculator.setTermInMonths("0");
        sh.tab();

        assertError(loanCalculator.termInMonthsError, loanCalculator.getTermInMonthsError(), "Please enter a value between 1 and 480");

        loanCalculator.setTermInMonths("481");
        sh.tab();

        assertError(loanCalculator.termInMonthsError, loanCalculator.getTermInMonthsError(), "Please enter a value between 1 and 480");

        loanCalculator.setTermInMonths("30");
        sh.tab();

        sh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(loanCalculator.termInMonthsError));
    }

    @Test
    public void checkInterestRateInputField()
    {
        // Assert that Interest Rate Per Year Input Field displays error message when entering a non numeric character
        loanCalculator.setInterestRatePerYear("a");
        sh.tab();

        assertError(loanCalculator.interestRatePerYearError, loanCalculator.getInterestRatePerYearError(), "Please enter a valid number.");

        loanCalculator.setInterestRatePerYear("####");
        sh.tab();

        assertError(loanCalculator.interestRatePerYearError, loanCalculator.getInterestRatePerYearError(), "Please enter a valid number.");

        // Assert that Interest Rate Per Year Input Field displays error message when entering a number that is not between 0.001 and 99
        loanCalculator.setInterestRatePerYear("0");
        sh.tab();

        assertError(loanCalculator.interestRatePerYearError, loanCalculator.getInterestRatePerYearError(), "Please enter a value between 0.001 and 99");

        loanCalculator.setInterestRatePerYear("100");
        sh.tab();

        assertError(loanCalculator.interestRatePerYearError, loanCalculator.getInterestRatePerYearError(), "Please enter a value between 0.001 and 99");

        loanCalculator.setInterestRatePerYear("30");
        sh.tab();

        sh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(loanCalculator.termInMonthsError));
    }

    private void assertError(By by, String actualError, String expectedError)
    {
        sh.wait(2).until(ExpectedConditions.visibilityOfElementLocated(by));
        assertTrue(actualError.equalsIgnoreCase(expectedError),
                "Expected: " + expectedError + "\nActual: " + actualError);
    }

}
