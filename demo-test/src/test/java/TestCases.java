import helpers.SeleniumHelper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.LoanCalculator;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by luisferrer
 */
public class TestCases extends BaseSeleniumDemo{

    @Test
    public void CorrectCaseTest()
    {
        DateTime time = new DateTime(2015, 9, 23, 12, 23);

        LoanCalculator loan = new LoanCalculator(driver, sh);
        loan.load();

        loan.setLoanAmount("10000")
                .setLoanTermInYears("30")
                .setInterestRatePerYear("7.9")
                .setLoanStartDate(time)
                .clickCalculate();

        sh.wait(15).until(ExpectedConditions.textToBePresentInElementLocated(loan.monthlyPayments, "$72.68"));

        assertEquals(loan.getMonthlyPayments(), "$72.68");
        assertEquals(loan.getLoanAmount(), "10,000");
        assertEquals(loan.getLoanTermInYears(),"30.000");
        assertEquals(loan.getTermInMonths(),"360");
        assertEquals(loan.getInterestRatePerYear(),"7.900");
        assertEquals(loan.getLoanStartDate(),"23 Sep 2015");
    }

    @Test
    public void IncorrectCaseTest()
    {
        DateTime time = new DateTime(2015, 9, 23, 12, 23);

        LoanCalculator loan = new LoanCalculator(driver, sh);
        loan.load();

        loan.setLoanAmount("15000")
                .setLoanTermInYears("30")
                .setInterestRatePerYear("7.9")
                .setLoanStartDate(time)
                .clickCalculate();

        sh.wait(15).until(ExpectedConditions.textToBePresentInElementLocated(loan.monthlyPayments, "$72.68"));

        assertEquals(loan.getMonthlyPayments(), "$72.68");
        assertEquals(loan.getLoanAmount(), "10,000");
        assertEquals(loan.getLoanTermInYears(),"30.000");
        assertEquals(loan.getTermInMonths(),"360");
        assertEquals(loan.getInterestRatePerYear(),"7.900");
        assertEquals(loan.getLoanStartDate(),"23 Sep 2015");
    }
}
